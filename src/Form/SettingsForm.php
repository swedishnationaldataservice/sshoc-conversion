<?php

namespace Drupal\sshoc_conversion\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Site\Settings;

/**
 * Configure SSHOC Settings
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'sshoc_conversion_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'sshoc_conversion.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config('sshoc_conversion.settings');

    $form['sshoc-server-backend-url'] = [
      '#type'          => 'textfield',
      '#title'         => t('SSHOC Server Backend URL'),
      '#default_value' => $config->get('sshoc-server-backend-url'),
      '#description'   => t("This is where SSHOC Conversion form sends data for conversion."),
    ];

    return parent::buildForm($form, $form_state);

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    \Drupal::configFactory()->getEditable('sshoc_conversion.settings')
      ->set('sshoc-server-backend-url', $form_state->getValue('sshoc-server-backend-url'))->save();
    parent::submitForm($form, $form_state);

  }

}
