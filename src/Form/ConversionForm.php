<?php

namespace Drupal\sshoc_conversion\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Url;
use GuzzleHttp\Client;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;

/**
 * Catalogue search form.
 */
class ConversionForm extends FormBase {

  protected $httpClient;
  
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'sshoc_conversion_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $current_path = \Drupal::service('path.current')->getPath();
    $path_args = explode('/', $current_path);
    $query = \Drupal::request()->query;
    
    $example = '<?xml version="1.0" encoding="UTF-8"?>
    <DDIInstance xmlns="ddi:instance:3_1" xmlns:s="ddi:studyunit:3_1" xmlns:r="ddi:reusable:3_1" xmlns:ddi="ddi:instance:3_1" isMaintainable="true" version="1.0.0" versionDate="2011-01-17" agency="se.snd">
        <s:StudyUnit id="0228-001" version="1.0.0" agency="se.snd">
            <r:Citation>
                <r:Title translatable="true" translated="false" xml:lang="sv">Riks-SOM 1987</r:Title>
                <r:Title translatable="true" translated="true" xml:lang="en">National SOM 1987</r:Title>
                <r:AlternateTitle translatable="true" translated="false" xml:lang="sv">SOM-undersökningen 1987</r:AlternateTitle>
                <r:Creator affiliation="Göteborgs universitet, Statsvetenskapliga institutionen" xml:lang="sv">Holmberg, Sören</r:Creator>
                <r:Creator affiliation="University of Gothenburg, Department of Political Science" xml:lang="en">Holmberg, Sören</r:Creator>
                <r:Creator affiliation="Göteborgs universitet, Statsvetenskapliga institutionen" xml:lang="sv">Weibull, Lennart</r:Creator>
                <r:Creator affiliation="University of Gothenburg, Department of Political Science" xml:lang="en">Weibull, Lennart</r:Creator>
            </r:Citation>
            <s:Abstract id="abstract1">
                <r:Content xml:lang="sv">This dataset contains information from the second SOM-survey. The questionnaire was divided into six subject fields: Mass media, politics and society, energy and nuclear power, Sweden´s relation to other countries, residential environment and interests, and social background and music habits. Many of the questions are replications of questions asked in SOM86, some of the questions put in SOM86 are excluded and a number of questions are added. These new questions deal with: important content in morning papers; etical rules in news service; advertisement; the most important problems of society; quality of party leaders; privatisation; government control of municipalities, trade and industry, and the individual; confidence in different groups in regard to foreign policy; government consideration to different groups in regard to foreign policy; televison treatment of those in power in a number of countries; comparisons between morning papers and television; respondent´s activities during the last 12 months; statements about respondent´s work; changes in the economic situation for the respondent and for the country; and music listening.</r:Content>
            </s:Abstract>
            <s:Abstract id="abstract2">
                <r:Content xml:lang="en">This dataset contains information from the second SOM-survey. The questionnaire was divided into six subject fields: Mass media, politics and society, energy and nuclear power, Sweden´s relation to other countries, residential environment and interests, and social background and music habits. Many of the questions are replications of questions asked in SOM86, some of the questions put in SOM86 are excluded and a number of questions are added. These new questions deal with: important content in morning papers; etical rules in news service; advertisement; the most important problems of society; quality of party leaders; privatisation; government control of municipalities, trade and industry, and the individual; confidence in different groups in regard to foreign policy; government consideration to different groups in regard to foreign policy; televison treatment of those in power in a number of countries; comparisons between morning papers and television; respondent´s activities during the last 12 months; statements about respondent´s work; changes in the economic situation for the respondent and for the country; and music listening.</r:Content>
            </s:Abstract>
            <r:Coverage>
                <r:TemporalCoverage id="temporalcoverage1">
                    <r:ReferenceDate>
                        <r:StartDate>1987-10-14</r:StartDate>
                        <r:EndDate>1988-02-10</r:EndDate>
                    </r:ReferenceDate>
                </r:TemporalCoverage>
            </r:Coverage>
        </s:StudyUnit>
    </DDIInstance>';

    $form['transformation'] = [
      '#type' => 'select',
      '#title' => $this->t('Transformation'),
      '#options' => $this->getTransformationList(),
      '#default_value' => 'ddi-3.1-to-dcterms'
    ];

    $form['xml_file'] = [
      '#title' => $this->t('Source XML'),
      '#type' => 'file',
      '#attributes' => ['accept' => '.xml'],
    ];

    $form['sshoc_xml_data'] = [
      '#type'          => 'textarea',
      '#title' => $this->t('Source XML'),
      '#attributes'    => ['placeholder' => t("Transform XML to SSHOC"), 'class' => ['sshoc-xml-textarea']],
      '#default_value' => $example,
      '#required'      => TRUE,
    ];

    $form['submit'] = [
      '#type'       => 'submit',
      '#value'      => t('Transform'),
      '#attributes' => ['class' => ['sshoc-submit-button']],
    ];

    // Placeholder to put the result of Ajax call, setMessage().
    $form['message'] = [
      '#type' => 'markup',
      '#markup' => '<div class="sshoc-submit-message" id="sshoc-status-message"></div>'
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $client = \Drupal::httpClient();
    $xmlData = trim($form_state->getValue('sshoc_xml_data'));
    $transformation = $form_state->getValue('transformation');
    $sshocBackend = self::getBackendUrl() . "/transform/" . $transformation;

    try {
      $response = $client->post($sshocBackend, [
        'body' => $xmlData,
        'http_errors' => TRUE
      ]);  
    } catch (RequestException $e) {

      \Drupal::logger('sshoc_conversion_module')
        ->error('RequestException on request %request message: %message', [
          '%request' => $request_url,
          '%message' => $e->getMessage(),
        ]);

      return (object)[];
    }

    if($response->getStatusCode() == 200) {

      $dt = new \Drupal\Core\Datetime\DrupalDateTime();
      $fileName = 'sshoc-converted-'.$dt->format('Y-m-d_Hi').'.xml';
      $resultContent = $this->contentDownload($response->getBody()->getContents(), 'text/xml', $fileName);
      $form_state->setResponse($resultContent);

      return $form;

    } else {
      \Drupal::logger('sshoc_conversion_module')->info('%code response on request %request ', [
        '%code' => $response->getStatusCode(),
        '%request' => $request_url,
      ]);
    }
  }

  function contentDownload($content, $content_type, $filename) {

    $form['#attached']['drupalSettings']['submit-millet'] = 'content-download-millet';
    $response = new Response();

    // Set headers
    $response->headers->set('Pragma', 'no-cache');
    $response->headers->set('Content-type', $content_type);
    $response->headers->set('Content-Disposition', 'attachment; filename="' . basename($filename) . '"');
    $response->headers->set('Cache-control', 'private');
    $response->headers->set('Content-length', strlen($content));
    $response->setContent($content);

    return $response;
  }

  private function getBackendUrl(): string{
    $requestUrl = \Drupal::config('sshoc_conversion.settings')->get('sshoc-server-backend-url');
    if(empty($requestUrl)){
      return 'http://sshoc-conversion-snd-dev.apps.k8s.gu.se';
    }

    return $requestUrl;
  }

  private function getTransformationList(){
    $client = \Drupal::httpClient();
    $requestUrl = self::getBackendUrl() . "/transformation-list";
    try {
      $response = $client->get($requestUrl);  
    } catch (RequestException $e) {

      \Drupal::logger('sshoc_conversion_module')
        ->error('RequestException on request %request message: %message', [
          '%request' => $requestUrl,
          '%message' => $e->getMessage(),
        ]);

      return (object)[];
    }

    if($response->getStatusCode() == 200) {

      $transformations = json_decode($response->getBody()->getContents());
      $result = array();
      foreach($transformations as $transformation){
        if($transformation->identifier){
          $result[$transformation->identifier] = $transformation->title;
        }
      }
      ksort($result);
      return $result;
    } else {
      \Drupal::logger('sshoc_conversion_module')->info('%code response on request %request ', [
        '%code' => $response->getStatusCode(),
        '%request' => $requestUrl,
      ]);

    }
  
  }
}
