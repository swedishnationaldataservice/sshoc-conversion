<?php

namespace Drupal\sshoc_conversion\Plugin\Block;
use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'SSHOC Conversion Form' Block.
 * 
 * To add 'SSHOC Conversion block' go to Structure -> Block Layout (admin/structure/block) and 
 * click on 'Place Block' button associated with each available region. Look for SSHOC
 *
 * @Block(
 *   id = "sshoc_conversion_block",
 *   admin_label = @Translation("SSHOC Conversion block"),
 *   category = @Translation("SSHOC Conversion Block"),
 * )
 */
class SSHOCConversionFormBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {

    $builtForm = \Drupal::formBuilder()->getForm('Drupal\sshoc_conversion\Form\ConversionForm');
    $renderArray['form'] = $builtForm;
    return $renderArray;

  }

}