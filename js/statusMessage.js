jQuery(function() {

  jQuery('#sshoc-status-message').hide();

  jQuery('#sshoc-conversion-form').submit(function() {
    jQuery('#sshoc-status-message').text(Drupal.t('Check downloaded xml file in your browser.'));
    jQuery('#sshoc-status-message').show(300);  
    return;
  });

  jQuery('#edit-xml-file').change(function(){
    var reader = new FileReader();
    var file = document.getElementById("edit-xml-file").files[0];
    reader.onload = function (e) {
      var textArea = document.getElementById("edit-sshoc-xml-data");
      textArea.value = e.target.result;
    };
    reader.readAsText(file);
  })



 });