# SSHOC DDI Conversion drupal module

Requires Drupal 8.x or 9.x

# Demo
Demo install at SND: https://snd.gu.se/en/sshoc-form

# About
Send conversion requests to sshoc conversion platform: 
https://bitbucket.org/swedishnationaldataservice/sshoc-conversion-platform

Test backend located at: http://sshoc-conversion-snd-dev.apps.k8s.gu.se
API documentation http://sshoc-conversion-snd-dev.apps.k8s.gu.se/swagger